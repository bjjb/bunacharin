.PHONY: image

main:
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .
image: main
	docker build -t bjjb/bunachareen .
push: image
	docker push bjjb/bunachareen
