package storage

import (
	"database/sql"
	"fmt"
)

// A DB is a bunacharín key/value store
type DB struct {
	db                *sql.DB
	table, key, value string
}

// NewDB opens a DB store, by opening the database specified by driver and
// name (see sql.Open), and setting the name of the table and the key and
// value columns.
func NewDB(driver, name, table, key, value string) (*DB, error) {
	db, err := sql.Open(driver, name)
	if err != nil {
		return nil, err
	}
	newDB := &DB{db, table, key, value}
	if err := newDB.init(); err != nil {
		return nil, err
	}
	return newDB, nil
}

// Get retrieves a value from the store for key
func (db *DB) Get(key string) ([]byte, error) {
	value := make([]byte, 1024)
	row := db.db.QueryRow(fmt.Sprintf("SELECT %s FROM %s WHERE %s = ? LIMIT 1",
		db.value, db.table, db.key), key)
	err := row.Scan(&value)
	switch err {
	case nil:
		return value, nil
	case sql.ErrNoRows:
		return nil, ErrNotFound
	default:
		return nil, err
	}
}

// Set stores a value at key
func (db *DB) Set(key string, val []byte) error {
	exists, err := db.exists(key)
	if err != nil {
		return err
	}
	if exists {
		return db.update(key, val)
	}
	return db.insert(key, val)
}

func (db *DB) insert(key string, val []byte) error {
	_, err := db.db.Exec(fmt.Sprintf("INSERT INTO %s (%s, %s) VALUES (?, ?)",
		db.table, db.key, db.value), key, val)
	return err
}

func (db *DB) update(key string, val []byte) error {
	_, err := db.db.Exec(fmt.Sprintf("UPDATE %s SET %s = ? WHERE %s = ?",
		db.table, db.value, db.key), val, key)
	return err
}

func (db *DB) exists(key string) (bool, error) {
	var value bool
	row := db.db.QueryRow(fmt.Sprintf("SELECT COUNT(%s) != 0 FROM %s WHERE %s = ?",
		db.key, db.table, db.key), key)
	err := row.Scan(&value)
	switch err {
	case nil:
		return value, nil
	case sql.ErrNoRows:
		return value, ErrNotFound
	default:
		return value, err
	}
}

func (db *DB) init() error {
	if _, err := db.db.Exec(fmt.Sprintf("CREATE TABLE IF NOT EXISTS %s (%s VARCHAR(255) NOT NULL UNIQUE, %s BLOB)", db.table, db.key, db.value)); err != nil {
		return err
	}
	return nil
}
