package storage

import "errors"

var (
	// ErrNotFound may be used to signal that the key doesn't exist
	ErrNotFound = errors.New("not found")
)

// Store is the interface to the simple key/value store
type Store interface {
	// Get returns the value for the given key or errors out
	Get(key string) ([]byte, error)
	// Set sets the value for the given key
	Set(key string, value []byte) error
}
