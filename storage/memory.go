package storage

import "sync"

type memory struct {
	table map[string][]byte
	lock  sync.RWMutex
}

// NewMemory creates a new DB which stores all data in memory.
func NewMemory() Store {
	return &memory{table: make(map[string][]byte)}
}

// Get safely retrieves a value for the key from memory
func (db *memory) Get(key string) ([]byte, error) {
	db.lock.RLock()
	defer db.lock.RUnlock()
	if value, ok := db.table[key]; ok {
		return value, nil
	}
	return nil, ErrNotFound
}

// Set safely stores the value at key; probably won't ever return an error
func (db *memory) Set(key string, val []byte) error {
	db.lock.Lock()
	defer db.lock.Unlock()
	db.table[key] = val
	return nil
}
