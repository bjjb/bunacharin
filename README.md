# Bunacharín

/bənəkərin/ (little store)

A simple key-value server.

## Installation

    go install bitbucket.org/bjjb/bunacharín

## Usage

    bunacharín

(If you're having difficulty with the 'í', it's usually
<kbd>Alt-GR</kbd>+<kbd>i</kbd>; or just use <kbd>Tab</kbd>)

You can specify a different TCP port with the `PORT` environment variable, or
with the `-address` flag.

Bunacharín will use an in-memory database. However, it's easy to specify a
different database backend; there are several example programs in the examples
folder, using Sqlite3, Redis, Postgres, etc.

To store the value "Hello" for the key "greet", using (say) curl:

    curl -XPUT -d 'Hello' http://localhost:6239/greet

You can then retrieve that value with

    curl http://localhost:6239/greet

Pretty simple, really.

## Credits and acknowledgements

Bunacharín is based on the work of [Go In 5 Minutes][goin5].

It's available under the MIT license.

[goin5]: https://goin5minutes.com/
