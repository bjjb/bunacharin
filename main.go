package main

import (
	"flag"
	"log"
	"net/http"
	"os"

	"bitbucket.org/bjjb/bunacharín/handlers"
	"bitbucket.org/bjjb/bunacharín/storage"
)

var port string
var address *string

func init() {
	// try to read the port from the environment
	if port = os.Getenv("PORT"); port == "" {
		// fall back if it's not specified
		port = "6239"
	}
	// the address will be set by a flag; the default is to use the port
	address = flag.String("address", ":"+port, "TCP listen address")

}

func main() {
	// router which routes to the right handler according to the request
	methodRouter := handlers.NewMethodRouter()
	// create the backend storage system
	store := storage.NewMemory()
	// register the Get handler
	methodRouter.Get(handlers.Get(store))
	// register the Put handler
	methodRouter.Put(handlers.Put(store))
	// create the mux with which to register our method-router
	mux := http.NewServeMux()
	// register the method router
	mux.Handle("/", methodRouter)
	// parse the command-line flags, to fill in the address
	flag.Parse()
	// notify that we're starting
	log.Printf("listening on %s", *address)
	// serve!
	log.Fatal(http.ListenAndServe(*address, mux))
}
