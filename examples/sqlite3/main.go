package main

import (
	"log"
	"net/http"

	"bitbucket.org/bjjb/bunacharín/handlers"
	"bitbucket.org/bjjb/bunacharín/storage"
	_ "github.com/mattn/go-sqlite3"
)

func init() {
	store, err := storage.NewDB("sqlite3", ":memory:", "blobs", "key", "value")
	if err != nil {
		log.Fatal(err)
	}
	route := handlers.NewMethodRouter()
	route.Get(handlers.Get(store))
	route.Put(handlers.Put(store))
	http.Handle("/", route)
}

func main() {
	log.Printf("bunacharín sqlite3 example listening on :8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
