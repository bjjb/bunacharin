package handlers

import (
	"fmt"
	"net/http"
)

// A MethodRouter is a http.Handler which will route requests to another
// http.Handler depending on the request method.
type MethodRouter interface {
	http.Handler
	// Register the handler for GET requests
	Get(http.Handler)
	// Register the handler for PUT requests
	Put(http.Handler)
	// Register the handler for POST requests
	Post(http.Handler)
	// Register the handler for HEAD requests
	Head(http.Handler)
	// Register the handler for INFO requests
	Info(http.Handler)
	// Register the handler for TRACE requests
	Trace(http.Handler)
	// Register the handler for DELETE requests
	Delete(http.Handler)
	// Register the handler for OPTIONS requests
	Options(http.Handler)
}

// methodRouter is a concrete implementation of a MethodRouter
type methodRouter struct {
	handlers map[string]http.Handler
}

// ServeHTTP implements the http.Handler interface for methodRouter
func (m *methodRouter) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if handler, ok := m.handlers[r.Method]; ok {
		handler.ServeHTTP(w, r)
	} else {
		http.Error(w, fmt.Sprintf("no handler for method %s", r.Method), http.StatusBadRequest)
	}
}

func (m *methodRouter) Get(h http.Handler)     { m.handlers["GET"] = h }
func (m *methodRouter) Put(h http.Handler)     { m.handlers["PUT"] = h }
func (m *methodRouter) Post(h http.Handler)    { m.handlers["POST"] = h }
func (m *methodRouter) Head(h http.Handler)    { m.handlers["HEAD"] = h }
func (m *methodRouter) Info(h http.Handler)    { m.handlers["INFO"] = h }
func (m *methodRouter) Trace(h http.Handler)   { m.handlers["TRACE"] = h }
func (m *methodRouter) Delete(h http.Handler)  { m.handlers["DELETE"] = h }
func (m *methodRouter) Options(h http.Handler) { m.handlers["OPTIONS"] = h }

// NewMethodRouter returns a MethodRouter whose handlers are empty for all
// methods.
func NewMethodRouter() MethodRouter {
	return &methodRouter{handlers: make(map[string]http.Handler)}
}
