package handlers

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"bitbucket.org/bjjb/bunacharín/storage"
)

// Put takes a DB and returns a HTTP Handler which stores the request body
// in the store using the request path as the key, and writes the response.
// Possible HTTP errors are BadRequest (if the path or body is empty) and an
// internal error if store.Set fails.
func Put(store storage.Store) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		key := r.URL.Path
		if key == "" {
			http.Error(w, "missing key name in path", http.StatusBadRequest)
			return
		}
		defer r.Body.Close()
		val, err := ioutil.ReadAll(r.Body)
		if err != nil {
			http.Error(w, "error reading request body", http.StatusBadRequest)
			return
		}
		if err := store.Set(key, val); err != nil {
			fmt.Fprintf(os.Stderr, "error writing to store: %s", err)
			http.Error(w, "error storing value", http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
	})
}
