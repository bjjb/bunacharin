package handlers

import (
	"fmt"
	"net/http"
	"os"

	"bitbucket.org/bjjb/bunacharín/storage"
)

// Get returns a http.Handler which uses the request path as a key, reads from
// the store, and writes the value to the response. Possible HTTP responses
// are OK, BadRequest (if the path is empty), NotFound (obviously), and
// InternalServerError if it found the value but failed to get it from the
// store.
func Get(store storage.Store) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		key := r.URL.Path
		if key == "" {
			http.Error(w, "missing key name in path", http.StatusBadRequest)
			return
		}
		val, err := store.Get(key)
		switch {
		case err == storage.ErrNotFound:
			http.Error(w, "not found", http.StatusNotFound)
		case err != nil:
			fmt.Fprintf(os.Stderr, "error reading from store: %s", err)
			http.Error(w, "error reading store", http.StatusInternalServerError)
		default:
			w.WriteHeader(http.StatusOK)
			w.Write(val)
		}
	})
}
